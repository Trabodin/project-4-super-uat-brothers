﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillBox : MonoBehaviour {
    public Transform tf;
    
	// Use this for initialization
	void Start () {
        tf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
		if(GameManager.instance.CurrentPlayer.pawn.gameObject.transform.position.y < tf.position.y)
        {
            GameManager.instance.LoseLife();
        }
	}
}
