﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour {

	public GameObject Message;
    public string SceneToload;
  
	// Use this for initialization
	void Start () {
		Message.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		
        if (Input.GetKeyDown(KeyCode.Return))
        {
            if(Message.activeInHierarchy == true)
            {

                GameManager.instance.NextLevel(SceneToload);
            }
        }
	}
    //put the text up and takes it down 
	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Player") {
			Message.SetActive (true);
		}
	}
	void OnTriggerExit2D(Collider2D other){
		if (other.tag == "Player") {
			Message.SetActive (false);
		}
	}


}
