﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour {
    public int WaypointNumber;
    public bool Active;
    public Animator an;
    public int once = 0;

	// Use this for initialization
	void Start () {
        an = GetComponent<Animator>();
        GameManager.instance.Checkpoints.Add(this);
        WaypointNumber = GameManager.instance.Checkpoints.Count;
	}
	
	// Update is called once per frame
	void Update () {

        if ( once == 0)
        {
            if (GameManager.instance.CurrentPlayer.pawn.gameObject.transform.position.x > transform.position.x)
            {
  
                an.Play("Flagwave");
                //sets all the checkpoints to inactive then set the last one to active only does it once
                
                for (int i = GameManager.instance.Checkpoints.Count - 1; i > 0; i--)
                {
                    if (GameManager.instance.Checkpoints[i].Active == true)
                    {
                        GameManager.instance.Checkpoints[i].Active = false;
                    }
                }
                Active = true;
                once = 1;
            }

        }
      
	}
}
