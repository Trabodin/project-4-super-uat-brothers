﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour {

    [HideInInspector]
    public Transform tf;

    public float CameraMoveDistance;
    public float CameraScrollSpeed;

    // Use this for initialization
    void Start()
    {
        tf = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        /*  if ((GameManager.instance.CurrentPlayer.gameObject.transform.position.x - tf.position.x) < - CameraMoveDistance || (GameManager.instance.CurrentPlayer.gameObject.transform.position.x - tf.position.x) > CameraMoveDistance)
          {
              Vector3 Xdirection = new Vector3(GameManager.instance.CurrentPlayer.gameObject.transform.position.x -  tf.position.x , 0 , -10);
              Xdirection.Normalize();
              tf.Translate(new Vector3(CameraScrollSpeed* Xdirection.x , 0));

          }

          if ((GameManager.instance.CurrentPlayer.gameObject.transform.position.y - tf.position.y ) < -CameraMoveDistance || (GameManager.instance.CurrentPlayer.gameObject.transform.position.y - tf.position.y) > CameraMoveDistance)
          {
              Vector3 Ydirection = new Vector3(0, GameManager.instance.CurrentPlayer.gameObject.transform.position.y - tf.position.y, -10);
              Ydirection.Normalize();
              tf.Translate(new Vector3(0,CameraScrollSpeed * Ydirection.y));
         }*/
        tf.position = GameManager.instance.CurrentPlayer.pawn.gameObject.transform.position + new Vector3(0, 0, -10);
    }

}
