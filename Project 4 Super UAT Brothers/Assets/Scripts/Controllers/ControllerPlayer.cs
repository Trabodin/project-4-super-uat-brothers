﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerPlayer : Controller {

	// Use this for initialization
	void Start () {
        GameManager.instance.CurrentPlayer = this;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.D))
		{
			pawn.Move(1);
		}
		if (Input.GetKey(KeyCode.A))
		{
			pawn.Move(-1);
		}
		if (Input.GetKeyDown(KeyCode.Space))
		{
			pawn.Jump();
		}
        if (Input.GetMouseButtonDown(0))
        {
            
            pawn.Attack();
        }
	}
}
