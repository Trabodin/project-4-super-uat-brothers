﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PawnWingGuy : Pawn {
    public Transform tf;
    public Rigidbody2D rb;
    public SpriteRenderer sr;

    public float side;
    public int Dir = 1;
    // Use this for initialization
    void Start()
    {

        tf = GetComponent<Transform>();
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void Move(float direction)
    {
        //moves untill something is above or below it then it changes direction
        tf.transform.Translate(Vector3.up * Dir * MoveSpeed * Time.deltaTime);
        RaycastHit2D Hitinfo;
        Hitinfo = Physics2D.Raycast(tf.position, new Vector3(0, Dir, 0), side);

        if (Hitinfo.collider != null)
        {

            Dir = Dir * -1;
        }
    }


    public override void Jump()
    {

    }

    public override void Attack()
    {

    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            GameManager.instance.LoseLife();
        }
    }

}
