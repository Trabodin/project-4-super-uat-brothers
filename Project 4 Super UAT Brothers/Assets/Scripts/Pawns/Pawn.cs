﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pawn : MonoBehaviour {

	public float MoveSpeed;
	public float Jumpforce;

	public abstract void Move (float direction);
	public abstract void Jump ();
    public abstract void Attack();

}
