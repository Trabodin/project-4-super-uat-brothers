﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPawn : Pawn {

    public Transform tf;
    public Rigidbody2D rb;
    public Animator an;
    public SpriteRenderer sr;

    public AudioClip AudioClip;
    public AudioClip EnemyDeath;

    public bool isGrounded;
    public float groundedDistance;
    public float AttackDistance;

    public float Facing;
    public int CurJump = 0;
    public int CurAtt = 0;

    // Use this for initialization
    void Start()
    {
        
        tf = GetComponent<Transform>();
        rb = GetComponent<Rigidbody2D>();
        an = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        CheckForGrounding();

        if(rb.velocity.y < 0)
        {
            an.Play("PlayerFall");
        }
    }

    public override void Move(float direction)
    {
        Facing = direction;

        tf.transform.Translate(Vector3.right * direction * MoveSpeed * Time.deltaTime);
        if (isGrounded )
        {
            an.Play("PlayerRun");
        }
        
        //fliping the animation in the direction of movement
        if (direction < 0)
        {
            sr.flipX = true;
        }
        else
        {
            sr.flipX = false;
        }
    }

    public override void Jump()
    {
        
        if (isGrounded || CurJump <= GameManager.instance.NumberOfJumps)
        {
            //kinda want to use vector decay
            CurJump++;
            ;

            rb.AddForce(Vector2.up * Jumpforce);

            if (CurJump == 1)
            {
                an.Play("Player_Jump1");
            }else if(CurJump > 1)
            {
                an.Play("PlayerSomesault");
            }
            
        }
    }

    public override void Attack()
    {
        CurAtt++;
        AudioSource.PlayClipAtPoint(AudioClip,tf.position);
        if(CurAtt == 1)
        {
            an.Play("PlayerAttack1");
        }
        if (CurAtt == 2)
        {
            an.Play("PlayerAttack2");
        }
        if (CurAtt == 3)
        {
            an.Play("PlayerAttack3");
            CurAtt = 0;
        }
        RaycastHit2D Hitinfo;
        Hitinfo = Physics2D.Raycast(tf.position, new Vector3(Facing,0,0), AttackDistance);

        if (Hitinfo.collider != null)
        {

            if (Hitinfo.collider.gameObject.tag == "Enemy")
            {
                AudioSource.PlayClipAtPoint(EnemyDeath, Hitinfo.collider.gameObject.transform.position);
                Destroy(Hitinfo.collider.gameObject);
            }
        }


    }

    public void CheckForGrounding()
    {
        RaycastHit2D Hitinfo;
        Hitinfo = Physics2D.Raycast(tf.position, Vector3.down, groundedDistance);

        if (Hitinfo.collider != null)
        {

            if (Hitinfo.collider.gameObject.tag == "Ground")
            {
                isGrounded = true;
                CurJump = 0;
            }
            else
            {
                isGrounded = false;
            }
        }
        else
        {
            isGrounded = false;
        }
    }


}
