﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Alpha2)) 
		{
			SceneManager.LoadScene ("Actual Level");
		}
		if (Input.GetKeyDown (KeyCode.Alpha1)) 
		{
			SceneManager.LoadScene ("Tortchtest");
		}
		if (Input.GetKeyDown (KeyCode.Alpha0)) 
		{
			SceneManager.LoadScene ("Scene1");
		}
	}
}
