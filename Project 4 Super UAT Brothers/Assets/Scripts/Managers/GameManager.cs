﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
	
	public static GameManager instance;

	[Header("Player")]
    public ControllerPlayer CurrentPlayer;

    public int MaxLives;
    public int CurLives;
    public int NumberOfJumps;

    public AudioClip audioclip;

    [Header("Checkpoints")]
    public int currentCheckpoint;
    public List<CheckPoint> Checkpoints;

	void Awake ()
	{
		if (instance == null) {
			instance = this;
			DontDestroyOnLoad (gameObject);
		} 
		else{
			Destroy (gameObject);
		}
	}

	// Use this for initialization
	void Start () {
        CurLives = MaxLives;
	}
	
	// Update is called once per frame
	void Update () {
		
        if (CurLives <= 0)
        {
            DoGameOver();
        }
	}

    public void LoseLife()
    {
        //sets the player back to the active checkpoint and minuses a life
        for (int i = Checkpoints.Count - 1; i > 0; i--)
        {
            if (Checkpoints[i].Active == true)
            {
                CurrentPlayer.pawn.gameObject.transform.position = Checkpoints[i].transform.position;
            }
        }
        CurLives--;
        AudioSource.PlayClipAtPoint(audioclip, CurrentPlayer.pawn.gameObject.transform.position);
    }

    public void NextLevel(string level)
    {
        //removes the check points from the list so there arnt empty spots on the list
        for (int i = Checkpoints.Count - 1; i > 0; i--)
        {
            Checkpoints.Remove(Checkpoints[i]);
        }
        SceneManager.LoadScene(level);
    }
    public void DoGameOver()
    {
        //removes the check points from the list so there arnt empty spots on the list
        for (int i = Checkpoints.Count - 1; i > 0; i--)
        {
            Checkpoints.Remove(Checkpoints[i]);
        }
        SceneManager.LoadScene("LossScreen");
        CurLives = MaxLives;
    }


    public void LoadGame()
    {
        SceneManager.LoadScene("Actual Level");
    }
    public void ReturnToMenu()
    {
        CurLives = MaxLives;
        SceneManager.LoadScene("MainMenu");
    }
}
