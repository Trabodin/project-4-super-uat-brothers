﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

	public AudioClip theClip;
	public AudioSource audioSource;

	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource> ();
		audioSource.clip = theClip;
		audioSource.Play ();
		//AudioSource.PlayOneShot ();//cant stop it after it starts
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
